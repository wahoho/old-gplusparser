<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.awsol.gplus.model.Member" %>
<%@ page import="com.awsol.gplus.dao.MemberDao" %>

<!DOCTYPE html>


<%@page import="java.util.ArrayList"%>

<html>
  <head>
    <title>Members</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
      <meta charset="utf-8"> 
  </head>
  <body>
<%
MemberDao dao = MemberDao.INSTANCE;

UserService userService = UserServiceFactory.getUserService();
User user = userService.getCurrentUser();

String url = userService.createLoginURL(request.getRequestURI());
String urlLinktext = "Login";
List<Member> memberlist = new ArrayList<Member>();
String userEmail = "";
Boolean isAdmin = false;
if (user != null){
    url = userService.createLogoutURL(request.getRequestURI());
    urlLinktext = "Logout";
    memberlist = dao.listMembers();
    userEmail = user.getEmail();
    isAdmin = Arrays.asList(new String[] {"ryandennisonchan@gmail.com", "shiki.roa@gmail.com", "wahoho.yap@gmail.com", "yapjason48@gmail.com"}).contains(userEmail);
}
    
%>
  <div style="width: 100%;">
    <div class="line"></div>
    <div class="topLine">
      <div style="float: left;"><img src="images/logo.png" /></div>
      <div style="float: left;" class="headline">Members</div>
      <div style="float: right;"><a href="<%=url%>"><%=urlLinktext%></a> <%=(user==null? "" : user.getNickname())%></div>
    </div>
  </div>

<div style="clear: both;"/>  <br/> <br />
You have a total number of <%= memberlist.size() %>  Members.

<table>
  <tr>
      <th>Name </th>
      <th>Google+ ID</th>
      <th>Pic</th>
      <th>Done</th>
    </tr>

<% for (Member member : memberlist) {%>
<tr> 
<td>
<%=member.getName()%>
</td>
<td>
<%=member.getGplusId()%>
</td>
<td>
<a href="<%= member.getProfileURL() %>"></a><img src="<%=member.getPicURL()%>?sz=50" /></a>
</td>
<% if (isAdmin) { %>
<td>
<a class="done" href="/done?id=<%=member.getId()%>" >Delete</a>
</td>
<%}%>
</tr> 
<%}
%>
</table>


<hr />

<div class="main">

<div class="headline">New Member</div>

<% if (user != null && isAdmin){ %> 

<form action="/new" method="post" accept-charset="utf-8">
  <table>
    <tr>
      <td><label for="gplusid">Google+ ID</label></td>
      <td><input type="text" name="gplusid" id="gplusid" size="35"/></td>
    </tr>
  <tr>
      <td colspan="2" align="right"><input type="submit" value="Create"/></td>
    </tr>
  </table>
</form>

<% }else{ %>

Please login with your Google account

<% } %>
</div>
</body>
</html> 