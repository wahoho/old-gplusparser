<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.awsol.gplus.model.Member" %>
<%@ page import="com.awsol.gplus.model.Post" %>
<%@ page import="com.awsol.gplus.dao.MemberDao" %>
<%@ page import="com.awsol.gplus.dao.PostDao" %>

<!DOCTYPE html>


<%@page import="java.util.ArrayList"%>

<html>
  <head>
    <title>Posts</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
      <meta charset="utf-8"> 
  </head>
  <body>
<%
MemberDao dao = MemberDao.INSTANCE;
PostDao postdao = PostDao.INSTANCE;

UserService userService = UserServiceFactory.getUserService();
User user = userService.getCurrentUser();

String url = userService.createLoginURL(request.getRequestURI());
String urlLinktext = "Login";
List<Post> postlist = new ArrayList<Post>();
String userEmail = "";
Boolean isAdmin = false;
if (user != null){
    url = userService.createLogoutURL(request.getRequestURI());
    urlLinktext = "Logout";
    postlist = postdao.listPosts();
    userEmail = user.getEmail();
    isAdmin = Arrays.asList(new String[] {"ryandennisonchan@gmail.com", "shiki.roa@gmail.com", "wahoho.yap@gmail.com", "yapjason48@gmail.com"}).contains(userEmail);
}
    
%>
  <div style="width: 100%;">
    <div class="line"></div>
    <div class="topLine">
      <div style="float: left;"><img src="images/logo.png" /></div>
      <div style="float: left;" class="headline">Members</div>
      <div style="float: right;"><a href="<%=url%>"><%=urlLinktext%></a> <%=(user==null? "" : user.getNickname())%></div>
    </div>
  </div>

<div style="clear: both;"/>  <br/> <br />
You have a total number of <%= postlist.size() %>  Posts.

<table>
  <tr>
  	  <th>No.</th>
      <th>URL </th>
      <th>Google+ ID</th>
      <th>Content</th>
      <th>Comments</th>
      <th>+1</th>
      <th>Published</th>
    </tr>


<% 
int x = 1;
for (Post post : postlist) {%>
<tr> 
<td>
<%=x++%>
</td>
<td>
<%=post.getUrl()%>
</td>
<td>
<%=post.getGplusId()%>
</td>
<td>
<%=post.getContent().getValue()%>
</td>
<td>
<%=post.getComments()%>
</td>
<td>
<%=post.getPlusOne()%>
</td>
<td>
<%=post.getPublished()%>
</td>
</tr> 
<%}
%>
</table>


<hr />

</body>
</html> 