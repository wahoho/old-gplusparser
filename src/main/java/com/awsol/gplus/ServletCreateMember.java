package com.awsol.gplus;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.awsol.gplus.dao.MemberDao;
import com.awsol.gplus.model.Member;
import com.awsol.gplus.service.GooglePlusService;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class ServletCreateMember extends HttpServlet {
  public void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    System.out.println("Creating new member ");
    User user = (User) req.getAttribute("user");
    if (user == null) {
      UserService userService = UserServiceFactory.getUserService();
      user = userService.getCurrentUser();
    } else {
    	String userEmail = user.getEmail();
    	Boolean isAdmin = Arrays.asList(new String[] {"ryandennisonchan@gmail.com", "shiki.roa@gmail.com", "wahoho.yap@gmail.com", "yapjason48@gmail.com"}).contains(userEmail);
    	if (!isAdmin) {
    		System.out.println("Not an admin");
    		resp.sendRedirect("/MemberMgr.jsp");
    	}
    }

    GooglePlusService gplussvc = new GooglePlusService();
    String userId = checkNull(req.getParameter("gplusid"));
    Member member = gplussvc.getUserInfo(userId);

    MemberDao.INSTANCE.add(member);

    resp.sendRedirect("/MemberMgr.jsp");
  }

  private String checkNull(String s) {
    if (s == null) {
      return "";
    }
    return s;
  }
} 