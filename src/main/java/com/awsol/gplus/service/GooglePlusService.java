package com.awsol.gplus.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.awsol.gplus.model.Member;
import com.awsol.gplus.model.Post;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.PlusRequestInitializer;
import com.google.api.services.plus.model.Activity;
import com.google.api.services.plus.model.Activity.PlusObject.Attachments;
import com.google.api.services.plus.model.Activity.PlusObject.Attachments.Thumbnails;
import com.google.api.services.plus.model.ActivityFeed;
import com.google.api.services.plus.model.Person;
import com.google.appengine.api.datastore.Text;

public class GooglePlusService {

	/**
	 * google plus service acess stub
	 */
	private Plus plusSvc;	

	/**
	 * google plus api key
	 */
	private static String GooglePlusAPIKey = "AIzaSyB36rOhRhjMQItTzWyPFDNjB7-EtvYF4JE"; 
	
	private static Pattern pattern = Pattern.compile("(.+/)w\\d{1,4}\\-h\\d{1,4}.+");	
	
	/**
	 * connect to the google plus service
	 */
	public GooglePlusService() {

		setupTransport();

	}

	public List<Activity> getActivityList(String userid) throws IOException {

		List<Activity> retval = new ArrayList<Activity>();

		Plus.Activities.List listActivities = plusSvc.activities().list(userid,
				"public");

		listActivities.setMaxResults(100L);

		// get the 1st page of activity objects
		ActivityFeed activityFeed = listActivities.execute();

		// unwrap the request and extract the pieces we want
		List<Activity> pageOfActivities = activityFeed.getItems();

		// loop through until we arrive at an empty page
		while (pageOfActivities != null) {
			for (Activity activity : pageOfActivities) {
				retval.add(activity);
				System.out.println("ID " + activity.getId() + " Content: "
						+ activity.getObject().getContent());
				// activity.getObject().getAttachments().
			}

			// we will know we are on the last page when the next page token
			// is null (in which case, break).
			if (activityFeed.getNextPageToken() == null) {
				break;
			}

			// prepare to request the next page of activities
			listActivities.setPageToken(activityFeed.getNextPageToken());

			// execute and process the next page request
			activityFeed = listActivities.execute();
			pageOfActivities = activityFeed.getItems();
			break; //will skip getting the next page. code above is obsolete.
		}

		return retval;

	}
	
	public Map<String, Post> getPosts(String userId) throws IOException {
		Map<String, Post> map = new HashMap<String, Post>();
		List<Activity> list = getActivityList(userId);
		
		for (Activity activity : list) {
			// get only post that were post by the 
			if ("post".equals(activity.getVerb())) {
				String id = activity.getId();
				String url = activity.getUrl();
				Text content = new Text(activity.getObject().getContent());
				Date published = new Date(activity.getPublished().getValue());
				Date updated = new Date(activity.getUpdated().getValue());
				Long comments = activity.getObject().getReplies().getTotalItems();
				Long plusOne = activity.getObject().getPlusoners().getTotalItems();
				List<Attachments> attachList =  activity.getObject().getAttachments();
				Boolean hasAlbum = false;
				String imageurl = "";
				List<String> imagesList = new ArrayList<String>();
				if (attachList != null) {
					for (Attachments attach : attachList) {
						String type = attach.getObjectType();
						if ("album".equals(type)) {
							hasAlbum = true;
							List<Thumbnails> thumbList = attach.getThumbnails();
							for (Thumbnails thumb : thumbList) {
								imageurl = thumb.getImage().getUrl();
								imageurl = stripHWFromGPlusImage(imageurl);
								imagesList.add(imageurl);
							}
						} else if ("photo".equals(type)) {
							imageurl = attach.getImage().getUrl();
							imageurl = stripHWFromGPlusImage(imageurl);
							imagesList.add(imageurl);
						}
						break;
					}
				}
				Post post = new Post(userId, id, url, content, published, updated, comments, plusOne, hasAlbum, imagesList);
				map.put(id, post);
			}
		}
		return map;
	}
	
	private String stripHWFromGPlusImage(String img) {
		Matcher matcher = pattern.matcher(img);
		String stripped = img;		
		if (matcher.matches()) {
			stripped = matcher.group(1);
		}
		
		return stripped;
	}

	public Member getUserInfo(String userId) throws IOException {
		Person target = null;

		target = plusSvc.people().get(userId).execute();

		String imageURL = target.getImage().getUrl();
		imageURL = imageURL.substring(0, imageURL.indexOf('?'));
		Member member = new Member(target.getDisplayName(), target.getId(),
				imageURL);
		member.setProfileURL(target.getUrl());

		System.out.println("ID:\t" + target.getId());
		System.out.println("Display Name:\t" + target.getDisplayName());
		System.out.println("Image URL:\t" + imageURL);
		System.out.println("Profile URL:\t" + target.getUrl());

		return member;
	}

	/**
	 * google plus service stub object
	 */
	public Plus getPlusSvc() {
		return plusSvc;
	}

	private void setupTransport() {

		GoogleCredential credential = new GoogleCredential();
		// plusSvc = new Plus(new NetHttpTransport(), new GsonFactory(),
		// credential);
		plusSvc = new Plus.Builder(new NetHttpTransport(), new GsonFactory(),
				credential)
				.setApplicationName("Meetrbus/1.0")
				.setHttpRequestInitializer(credential)
				.setPlusRequestInitializer(
						new PlusRequestInitializer(GooglePlusAPIKey)).build();

	}

}