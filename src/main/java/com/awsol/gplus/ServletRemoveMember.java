package com.awsol.gplus;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.awsol.gplus.dao.MemberDao;

public class ServletRemoveMember extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String id = req.getParameter("id");
		MemberDao.INSTANCE.remove(Long.parseLong(id));
		resp.sendRedirect("/MemberMgr.jsp");
	}

}
