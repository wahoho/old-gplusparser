package com.awsol.gplus.cron;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.awsol.gplus.dao.MemberDao;
import com.awsol.gplus.dao.PostDao;
import com.awsol.gplus.model.Member;
import com.awsol.gplus.model.Post;
import com.awsol.gplus.service.GooglePlusService;

import org.apache.commons.beanutils.BeanUtils;

public class ServletScrapePosts extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		List<Member> memberList = MemberDao.INSTANCE.listMembers();

		System.out.println("Scrape Start");
		for (Member member : memberList) {
			String gplusId = member.getGplusId();
			System.out.println("Scraping posts for " + member.getName());
			List<Post> list = PostDao.INSTANCE.listPosts(gplusId);
			System.out.println("Old posts: " + list.size());
			Map<String, Post> oldMap = convertListPostToMap(list);

			GooglePlusService gplussvc = new GooglePlusService();
			Map<String, Post> newMap = gplussvc.getPosts(gplusId);

			compareMaps(oldMap, newMap);
		}
		System.out.println("Scrape Done");
	}

	private Map<String, Post> convertListPostToMap(List<Post> list) {
		Map<String, Post> map = new HashMap<String, Post>();

		for (Post post : list) {
			String postId = post.getPostId();
			map.put(postId, post);
		}
		return map;
	}

	private void compareMaps(Map<String, Post> oldMap, Map<String, Post> newMap) {
		for (Map.Entry<String, Post> entry : newMap.entrySet()) {
			String postId = entry.getKey();
			Post newPost = entry.getValue();

			Post oldPost = oldMap.get(postId);
			if (oldPost != null) {
				System.out.println("Old Post. Not Inserting " + postId);
				if (oldPost.getUpdated() == null || !oldPost.getUpdated().equals(newPost.getUpdated())) {
					oldPost.setUpdated(newPost.getUpdated());
					oldPost.setPublished(newPost.getPublished());
					oldPost.setContent(newPost.getContent());
					oldPost.setComments(newPost.getComments());
					oldPost.setPlusOne(newPost.getPlusOne());
					oldPost.setHasAlbum(newPost.isHasAlbum());
					oldPost.setImagesList(newPost.getImagesList());
				} else {
					Long newComments = newPost.getComments();
					Long newPlusOne = newPost.getPlusOne();
					Long oldComments = oldPost.getComments();
					Long oldPlusOne = oldPost.getPlusOne();

					if (newComments != oldComments || newPlusOne != oldPlusOne) {
						oldPost.setComments(newComments);
						oldPost.setPlusOne(newPlusOne);
					}
				}
			} else {
				System.out.println("New Post. Inserting " + postId);
				PostDao.INSTANCE.add(newPost);
			}
		}
	}

}
