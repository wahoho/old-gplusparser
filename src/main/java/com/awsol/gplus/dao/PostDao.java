package com.awsol.gplus.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.awsol.gplus.model.Post;
import com.google.appengine.api.datastore.Text;

public enum PostDao {
	INSTANCE;

	private Query setupQueryForListPost(String memberId) {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("SELECT p FROM Post p WHERE p.gplusId = '"
				+ memberId + "'");
		return q;
	}

	public List<Post> listPosts() {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("SELECT p FROM Post p");
		List<Post> posts = q.getResultList();
		
		for (Post p : posts) {
			System.out.println(p.toString());
		}
		
		return posts;
	}
	
	public List<Post> listPosts(String memberId) {
		Query q = setupQueryForListPost(memberId);
		List<Post> posts = q.getResultList();
		return posts;
	}

	public List<Post> listPosts(String memberId, int max) {
		Query q = setupQueryForListPost(memberId);
		q.setMaxResults(max);
		List<Post> posts = q.getResultList();
		return posts;
	}

	public void Post(String userId, String postId, String url, Text content,
			Date published, Date updated, Long comments, Long plusOne, Boolean hasAlbum,
			List<String> imagesList) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			Post post = new Post(userId, postId, url, content, published, updated,
					comments, plusOne, hasAlbum, imagesList);
			em.persist(post);
			em.close();
		}
	}

	public void add(Post post) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			em.persist(post);
			em.close();
		}
	}

	public void remove(long id) {
		EntityManager em = EMFService.get().createEntityManager();
		try {
			Post post = em.find(Post.class, id);
			em.remove(post);
		} finally {
			em.close();
		}
	}
}