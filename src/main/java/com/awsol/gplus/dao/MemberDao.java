package com.awsol.gplus.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.awsol.gplus.model.Member;

public enum MemberDao {
	INSTANCE;

	public List<Member> listMembers() {
		EntityManager em = EMFService.get().createEntityManager();
		// read the existing entries
		Query q = em.createQuery("select m from Member m");
		List<Member> members = q.getResultList();
		return members;
	}

	public void add(String name, String gplusId, String picURL) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			Member member = new Member(name, gplusId, picURL);
			em.persist(member);
			em.close();
		}
	}

	public void add(Member member) {
		synchronized (this) {
			EntityManager em = EMFService.get().createEntityManager();
			em.persist(member);
			em.close();
		}
	}

	public void remove(long id) {
		EntityManager em = EMFService.get().createEntityManager();
		try {
			Member member = em.find(Member.class, id);
			em.remove(member);
		} finally {
			em.close();
		}
	}
}